require('dotenv').config();
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const requestJson = require('request-json');
const oracledb = require('oracledb');
const moment = require("moment");
const port = process.env.PORT ||3000;
const URL_BASE = '/apitechu/v2/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu5db/collections/';
const API_KEY = 'apiKey=' + process.env.API_KEY_MLAB;


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended :true}));

app.listen(port);
console.log('node js escuchando en el puerto '+ port);

app.get('/holamundos',function(req,res) {
  console.log('hola mundo');
  res.send('hola eduardo cuellar');//la respuseta se envia siempre en ultimo lugar porque se corta la comunicacion
});
// tabla transacciones
app.post(URL_BASE + 'movimientos',
  (request, response) => {
    let aorigen = request.body.account_origen;
    console.log(aorigen);
    let queryString = `q={"account_origen":"${aorigen}"}&`;
    //let fieldString = 'f={"_id":0,"account":1}&';
    let fieldString = 'f={"_id":0,"account_origen":0}&';
    const httpClient = requestJson.createClient(URL_MLAB);
    httpClient.get('transactions?'+queryString  + fieldString  + API_KEY, (error, resMlab, body) => {
      if (error) {
        res = { 'msg': 'Error en la peticion a mLab' };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = { 'msg': 'movimientos no encontrados' };
        }
      }
      response.send(res);
    });
  });

app.post(URL_BASE + "transfer",
  function (request, response) {
    let aorigen = request.body.account_origen;
    let adestino = request.body.account_destino;
    let importe = request.body.importe;
    let tcambio = request.body.tipo_cambio;
    let today = moment(new Date()).format('YYYY-MM-DD[T00:00:00.000Z]');
    console.log("POST origen = "+ aorigen+ "  importe = "+ importe);
    //let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let count_param = `user_account?${c=true}&`
    let newtransacciton = {
      "account_origen": aorigen,
      "importe_origen": importe,
      "account_destino": adestino,
      "importe_destino": importe,
      "tipo_cambio":tcambio,
      "fecha_mov":today
    };
    const httpClient = requestJson.createClient(URL_MLAB);
    httpClient.post('transactions?' + API_KEY, newtransacciton ,(error, resMlab, body) => {
      let res = {};
      if (error) {
        res = { 'msg': 'Error en la peticion a mLab' };
        response.status(500);
      } else {
        res = { 'msg': 'transferecia ejectuada' };
        response.status(201);
      }
      response.send(res);
    });
  });

//tabla account
app.get(URL_BASE + 'users/:id/accounts',
  (request, response) => {
    console.log(request.params.id);
    let id_user = request.params.id;
    let queryString = `q={"id_user":${id_user}}&`;
    //let fieldString = 'f={"_id":0,"account":1}&';
    let fieldString = 'f={"_id":0,"id_user":0}&';
    const httpClient = requestJson.createClient(URL_MLAB);
    httpClient.get('accounts?' + queryString + fieldString + API_KEY, (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = { 'msg': 'Error en la peticion a mLab' };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = { 'msg': 'Cuentas no encontradas' };
        }
      }
      response.send(res);
    });
  });
/// tabla user
  app.get(URL_BASE + 'users/:id',
    (request, response) => {
      console.log(request.params.id);
      let id_user = request.params.id;
      let queryString = `q={"id_user":${id_user}}&`;
      //let fieldString = 'f={"_id":0,"account":1}&';
      let fieldString = 'f={"_id":0,"id_user":0}&';
      const httpClient = requestJson.createClient(URL_MLAB);
      httpClient.get('user?' + queryString + fieldString + API_KEY, (error, resMlab, body) => {
        let res = {};
        if (error) {
          res = { 'msg': 'Error en la peticion a mLab' };
          response.status(500);
        } else {
          if (body.length > 0) {
            res = body;
          } else {
            response.status(404);
            res = { 'msg': 'Cuentas no encontradas' };
          }
        }
        response.send(res);
      });
    });

    app.get(URL_BASE + 'users/dni/:dni',
      (request, response) => {
        console.log(request.params.dni);
        let dni = request.params.dni;
        let queryString = `q={"dni":${dni}}&`;
        //let fieldString = 'f={"_id":0,"account":1}&';
        let fieldString = 'f={"_id":0,"dni":0}&';
        const httpClient = requestJson.createClient(URL_MLAB);
        httpClient.get('user?' + queryString + fieldString + API_KEY, (error, resMlab, body) => {
          let res = {};
          if (error) {
            res = { 'msg': 'Error en la peticion a mLab' };
            response.status(500);
          } else {
            if (body.length > 0) {
              res = body;
            } else {
              response.status(404);
              res = { 'msg': 'Cuentas no encontradas' };
            }
          }
          response.send(res);
        });
      });

      app.get(URL_BASE + 'users/cel/:cel',
        (request, response) => {
          console.log(request.params.dni);
          let cel = request.params.cel;
          let queryString = `q={"nrocel":${cel}}&`;
          //let fieldString = 'f={"_id":0,"account":1}&';
          let fieldString = 'f={"_id":0,"id_user":1}&';
          const httpClient = requestJson.createClient(URL_MLAB);
          httpClient.get('user?' + queryString + fieldString + API_KEY, (error, resMlab, body) => {
            let res = {};
            if (error) {
              res = { 'msg': 'Error en la peticion a mLab' };
              response.status(500);
            } else {
              if (body.length > 0) {
                res = body;
              } else {
                response.status(404);
                res = { 'msg': 'Cuentas no encontradas' };
              }
            }
            response.send(res);
          });
        });
  //Method POST login
  app.post(URL_BASE + "login",
    function (req, res) {
      let email = req.body.email;
      let pass = req.body.password;
      console.log("POST email = "+ email+ "  pass = "+ pass);
      let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
      let limFilter = 'l=1&';
      let clienteMlab = requestJson.createClient(URL_MLAB);
      clienteMlab.get('user?' + queryString + limFilter + API_KEY,
        function (error, respuestaMLab, body) {
          if (!error) {
            if (body.length == 1) { // Existe un usuario que cumple 'queryString'
              let login = '{"$set":{"logged":true}}';
              clienteMlab.put('user?q={"id_user": ' + body[0].id_user + '}&' + API_KEY, JSON.parse(login),
                //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
                function (errPut, resPut, bodyPut) {
                  res.send({ 'msg': 'Login correcto', 'user': body[0].email, 'userid': body[0].id_user, 'name': body[0].first_name });
                  // If bodyPut.n == 1, put de mLab correcto
                });
            }
            else {
              res.status(404).send({ "msg": "Usuario no válido." });
            }
          } else {
            res.status(500).send({ "msg": "Error en petición a mLab." });
          }
        });
    });

    //Method POST logout
    app.post(URL_BASE + "logout",
      function (req, res) {
        var email = req.body.email;
        console.log("POST email = "+ email);
        var queryString = 'q={"email":"' + email + '","logged":true}&';
        console.log(queryString);
        var clienteMlab = requestJson.createClient(URL_MLAB);
        clienteMlab.get('user?' + queryString + API_KEY,
          function (error, respuestaMLab, body) {
            var respuesta = body[0]; // Asegurar único usuario
            if (!error) {
              if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
                let logout = '{"$unset":{"logged":true}}';
                clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + API_KEY, JSON.parse(logout),
                  //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
                  function (errPut, resPut, bodyPut) {
                    res.send({ 'msg': 'Logout correcto', 'user': respuesta.email });
                    // If bodyPut.n == 1, put de mLab correcto
                  });
              } else {
                res.status(404).send({ "msg": "Logout failed!" });
              }
            } else {
              res.status(500).send({ "msg": "Error en petición a mLab." });
            }
          });
      });

////////////////////////////////////////////////////////////////////////////////////////////////////////
